#include "imu_ekf.h"

/*****************************************************/
/*                    main                           */
/*****************************************************/

int main(int argc, char* argv[]) {

    struct timeval before, after;

    srand(time(NULL));

    // Definition of the parameters and state variables
    ekf_param p;
    ekf_state s;
    // The parameters for the evolution equation
    s.params = gsl_vector_alloc(7); //for time, angular velocities and acceleration
    //s.params->data[0] = DT;

    // Initialization of the parameters
    p.n = 19; //for 3-angular velocities + 4 quaternion values, 3-position, 3-velocity, 3-acceleration , 3-bias
    p.no = 19; //same dimesnion as that of state

    //EvolutionNoise * evolution_noise = new ekf::EvolutionAnneal(1e-2, 0.99, 1e-8);
    EvolutionNoise * evolution_noise = new ekf::EvolutionRLS(1e-2, 0.9995);
    //EvolutionNoise * evolution_noise = new ekf::EvolutionRobbinsMonro(1e-5, 1e-6);
    p.evolution_noise = evolution_noise;

    p.observation_noise = 1e-1;

    p.prior_pk = 1.0;
    p.observation_gradient_is_diagonal = true;

    // Initialization of the state and parameters
    ekf_init(p,s);

    // Initialize the parameter vector to some random values
    for(int i = 0 ; i < p.n ; i++)
        gsl_vector_set(s.xk,i,0.01*rand()/double(RAND_MAX-1));
    // s.xk->data[0] = 0.0;
    // s.xk->data[1] = 0.0;
    // s.xk->data[2] = 0.0;

    // s.xk->data[3] = 0.5;
    // s.xk->data[4] = 0.5;
    // s.xk->data[5] = 0.5;
    // s.xk->data[6] = 0.5;
                                //set quaternion to 0,0,0,1 and acc and velocity to zero
                                //0,1,2 -> angular velocties
                                //3,4,5,6 -> quaternion (qx,qy,qz,qw)
    s.xk->data[3] = 0.0;
    s.xk->data[4] = 0.0;
    s.xk->data[5] = 0.0;
    s.xk->data[6] = 1.0;
                                //7,8,9 -> acceleration
                                //10,1,,12 -> velocity
                                //13,14,15 -> position
    s.xk->data[7] = 0.0;
    s.xk->data[8] = 0.0;
    s.xk->data[9] = 0.0;
    s.xk->data[10] = 0.0;
    s.xk->data[11] = 0.0;
    s.xk->data[12] = 0.0;
    s.xk->data[13] = 0.0;
    s.xk->data[14] = 0.0;
    s.xk->data[15] = 0.0;
                                //16,17,18 -> bias in acc.
    s.xk->data[16] = bx0;
    s.xk->data[17] = by0;
    s.xk->data[18] = bz0;

    // Allocate the input/output vectors
    gsl_vector * xi = gsl_vector_alloc(p.n);
    gsl_vector * yi = gsl_vector_alloc(p.no);
    gsl_vector_set_zero(yi);

    /***********************************************/
    /***** Iterate the learning on the samples *****/
    /***********************************************/

    int epoch = 0;

    //0,1,2 -> w == angular velocity
    //3,4,5,6 -> q == quaternions with the form qx,qy, qz, qw
    xi->data[0] = WX0;
    xi->data[1] = WY0;
    xi->data[2] = WZ0;
    xi->data[3] = QX0;
    xi->data[4] = QY0;
    xi->data[5] = QZ0;
    xi->data[6] = QW0;
    //7,8,9 -> acceleration in x,y,z axes
    //10,11,12 -> velocity in x,y,z axes
    //13,14,15 -> position in x,y,z axes
    xi->data[7] = AX0;
    xi->data[8] = AY0;
    xi->data[9] = AZ0;
    xi->data[10] = VX0;
    xi->data[11] = VY0;
    xi->data[12] = VZ0;
    xi->data[13] = PX0;
    xi->data[14] = PY0;
    xi->data[15] = PZ0;
    //16,17,18 -> bias in acceleration
    xi->data[16] = bx0;
    xi->data[17] = by0;
    xi->data[18] = bz0;
    

    //std::ifstream infile("angular_velocity.txt");

    
    std::ofstream outfile("imu_ekf.data");
    std::ofstream outfile_rms("imu_ekf-rms.data");
    double rms= 0.0;
    double errorBound = NOISE_AMPLITUDE/2.0;
    int count_time=0;
    int nb_steps_below_error=1000;
    //int is_counting=0;

    gettimeofday(&before, NULL);

    outfile << epoch << " ";
    for(int i = 0 ; i < p.n ; ++i)
        outfile << xi->data[i] << " " ;
    for(int i = 0 ; i < p.no ; ++i)
        outfile << yi->data[i] << " " ;

    double roll, pitch, yaw;
    toEulerAngle(s.xk->data[3],s.xk->data[4],s.xk->data[5],s.xk->data[6],roll, pitch, yaw);
    std::cout<<s.xk->data[3]<<" "<<s.xk->data[4]<<" "<<s.xk->data[5]<<" "<<s.xk->data[6]<<" "<<roll<<" "<< pitch<<" "<< yaw<<std::endl;
    for(int i = 0 ; i < 3 ; ++i)
        outfile << s.xk->data[i] << " " ;
    roll*=radiantodegree;
    pitch*=radiantodegree;
    yaw*=radiantodegree;
    outfile<<roll<<" "<<pitch<<" "<< yaw <<" " ;
    //outfile << std::endl;

    //for acc, vel and pos. and bias
    for(int i = 7 ; i < 19 ; ++i)
        outfile <<s.xk->data[i] <<" ";

    outfile<<"\n";

    double mean_wx,mean_wy,mean_wz;
    double mean_roll = 0, mean_pitch = 0, mean_yaw = 0;

    double ax_prev = 0.0;
    double ay_prev = 0.0;
    double az_prev = 0.0;
    
    //to get the T0, we read gyro and acc readings once
    double timestamp;
    double omega_x, omega_y, omega_z, a_x, a_y, a_z, bnk, hdg, atd ;
    get_gyro(timestamp, omega_x, omega_y, omega_z);
    get_gyro(timestamp, omega_x, omega_y, omega_z); //reading gyro twice because the acc. processed data is recorded at half the frequency of gyro raw
    double theta_x_0 = 0, theta_y_0 = 0, theta_z_0 = 0;
    double rw = 0, ptch = 0, yw = 0;
    /*
    get_orientation(timestamp,bnk,hdg,atd, theta_x_0, theta_y_0, theta_z_0); //for reading orientation
    theta_x_0 = bnk; theta_y_0 = hdg; theta_z_0 = atd;
    get_acc(timestamp, a_x, a_y, a_z);
    convert_acc(a_x,a_y,a_z, bnk,hdg,atd);
    */
    get_acc(timestamp, a_x, a_y, a_z);
    convert_acc(a_x,a_y,a_z, rw,ptch,yw);
    double t0 = timestamp;
    const double T0 = timestamp;
    //reduce_acc_error(a_x,a_y,a_z,ax_prev, ay_prev, az_prev);
       
    while( epoch < no_of_biaspredictor)
    {   
        get_gyro(timestamp, omega_x, omega_y, omega_z);
        get_gyro(timestamp, omega_x, omega_y, omega_z);
        get_orientation(timestamp,bnk,hdg,atd, theta_x_0, theta_y_0, theta_z_0); //for reading orientation
        //theta_x_0 = bnk; theta_y_0 = hdg; theta_z_0 = atd;
        get_acc(timestamp, a_x, a_y, a_z);
        convert_acc(a_x,a_y,a_z, rw,ptch,yw);
        
        s.params->data[0] = timestamp - t0;
        total_time = total_time + s.params->data[0];
        //std::cout <<s.params->data[0] << "\n";
        t0 = timestamp;

        s.params->data[1] = omega_x;
        s.params->data[2] = omega_y;
        s.params->data[3] = omega_z;

        s.params->data[4] = a_x;
        s.params->data[5] = a_y;
        s.params->data[6] = a_z;

        /*assigning previous and current one for the next iteration*/
        ax_prev = a_x; ay_prev = a_y; az_prev = a_z;
        /*assigning done*/

        f(s.params, xi, xi);
        h(s.params, xi, yi);
         
        // Provide the observation and iterate
        ekf_iterate(p,s,f,df,h,dh,yi);

        epoch++;

        rms = 0.0;
        for(int i = 0 ; i < p.n ; ++i)
            rms += gsl_pow_2(xi->data[i] - s.xk->data[i]);
        rms /= double(p.n);
        rms = sqrt(rms);
        if(rms <= errorBound)
        {
            count_time++;
            if(count_time >= nb_steps_below_error)
                break;
        }
        else
        {
            count_time = 0;
        }

        outfile <<t0 - T0 << " ";
        
        for(int i = 0 ; i < p.n ; ++i)
            outfile << xi->data[i] << " " ;
        for(int i = 0 ; i < p.no ; ++i)
            outfile << yi->data[i] << " " ;
        
        double roll, pitch, yaw;
        //std::cout<<s.xk->data[3]<<" "<<s.xk->data[4]<<" "<<s.xk->data[5]<<" "<<s.xk->data[6]<<std::endl;
        toEulerAngle(s.xk->data[3],s.xk->data[4],s.xk->data[5],s.xk->data[6],roll, pitch, yaw);
        std::cout<<s.xk->data[3]<<" "<<s.xk->data[4]<<" "<<s.xk->data[5]<<" "<<s.xk->data[6]<<" "<<roll<<" "<< pitch<<" "<< yaw<<std::endl;
        for(int i = 0 ; i < 3 ; ++i)
            outfile << s.xk->data[i] << " " ;
        roll*=radiantodegree;
        pitch*=radiantodegree;
        yaw*=radiantodegree;

        mean_roll  = (mean_roll  + roll )/epoch;
        mean_pitch = (mean_pitch + pitch)/epoch;
        mean_yaw   = (mean_yaw   + yaw  )/epoch;

        //std::cout<<"mean_roll = "<<mean_roll<<" "<<"mean_pitch = "<<mean_pitch<<" mean_yaw = "<<mean_yaw<<"\n";
        outfile<<roll - mean_roll <<" "<<pitch - mean_pitch<<" "<<yaw - mean_yaw<<" ";
        //outfile << std::endl;
        rw = (roll - mean_roll)/radiantodegree;
        ptch = (pitch - mean_pitch)/radiantodegree;
        yw = (yaw - mean_yaw)/radiantodegree;
        //for acc, vel and pos.
        for(int i = 7 ; i < 19 ; ++i)
            outfile << s.xk->data[i] << " " ;
        outfile<<"\n";
        //std::cout<<"dt = "<<s.xk->data[0]<<" "<<s.xk->data[10]<<" "<<s.xk->data[11]<<" "<<s.xk->data[12]<<s.xk->data[13]<<" "<<s.xk->data[14]<<" "<<s.xk->data[15]<<"\n";
        outfile_rms << rms << std::endl;
    }

    

    while(!acc_infile.eof()  )//epoch < no_of_iterations)
    {
        get_gyro(timestamp, omega_x, omega_y, omega_z);
        get_gyro(timestamp, omega_x, omega_y, omega_z);
        get_orientation(timestamp,bnk,hdg,atd, theta_x_0, theta_y_0, theta_z_0); //for reading orientation
        //theta_x_0 = bnk; theta_y_0 = hdg; theta_z_0 = atd;
        get_acc(timestamp, a_x, a_y, a_z);
        convert_acc(a_x,a_y,a_z, rw,ptch,yw);

        s.params->data[0] = timestamp - t0;
        total_time = total_time + s.params->data[0];
        //std::cout <<s.params->data[0] << "\n";
        t0 = timestamp;
        
        s.params->data[1] = omega_x;
        s.params->data[2] = omega_y;
        s.params->data[3] = omega_z;

        s.params->data[4] = a_x;
        s.params->data[5] = a_y;
        s.params->data[6] = a_z;

        /*assigning previous and current one for the next iteration*/

        ax_prev = a_x; ay_prev = a_y; az_prev = a_z;
  

        f(s.params, xi, xi);
        h(s.params, xi, yi);
        // Provide the observation and iterate
        ekf_iterate(p,s,f,df,h,dh,yi);

        epoch++;

        rms = 0.0;
        for(int i = 0 ; i < p.n ; ++i)
            rms += gsl_pow_2(xi->data[i] - s.xk->data[i]);
        rms /= double(p.n);
        rms = sqrt(rms);
        if(rms <= errorBound)
        {
            count_time++;
            if(count_time >= nb_steps_below_error)
                break;
        }
        else
        {
            count_time = 0;
        }

        outfile <<t0 - T0 << " ";
        
        for(int i = 0 ; i < p.n ; ++i)
            outfile << xi->data[i] << " " ;
        for(int i = 0 ; i < p.no ; ++i)
            outfile << yi->data[i] << " " ;

        mean_wx += s.xk->data[0];
        mean_wy += s.xk->data[1];
        mean_wz += s.xk->data[2];

        double roll, pitch, yaw;
        //std::cout<<s.xk->data[3]<<" "<<s.xk->data[4]<<" "<<s.xk->data[5]<<" "<<s.xk->data[6]<<std::endl;
        toEulerAngle(s.xk->data[3],s.xk->data[4],s.xk->data[5],s.xk->data[6],roll, pitch, yaw);
        std::cout<<s.xk->data[3]<<" "<<s.xk->data[4]<<" "<<s.xk->data[5]<<" "<<s.xk->data[6]<<" "<<roll<<" "<< pitch<<" "<< yaw<<std::endl;
        for(int i = 0 ; i < 3 ; ++i)
            outfile << s.xk->data[i] << " " ;
        roll*=radiantodegree;
        pitch*=radiantodegree;
        yaw*=radiantodegree;
        outfile<<roll - mean_roll <<" "<<pitch - mean_pitch <<" "<<yaw - mean_yaw<<" ";
        //outfile << std::endl;
        rw = (roll - mean_roll)/radiantodegree;
        ptch = (pitch - mean_pitch)/radiantodegree;
        yw = (yaw - mean_yaw)/radiantodegree;
        //for acc, vel and pos.
        for(int i = 7 ; i < 19 ; ++i)
            outfile << s.xk->data[i] << " " ;
        outfile<<"\n";
        outfile_rms << rms << std::endl;
    }
    gettimeofday(&after, NULL);
    double sbefore = before.tv_sec + before.tv_usec * 1E-6;
    double safter = after.tv_sec + after.tv_usec * 1E-6;
    double total = safter - sbefore;

    outfile.close();
    outfile_rms.close();
    std::cout<<"Angular velocity mean:"<<"\n"<<"<w_x> = "<< mean_wx/epoch <<" <w_y> = "<< mean_wy/epoch <<"<w_z> = "<< mean_wz/epoch << "\n";
    std::cout << " Run on " << epoch << " epochs ;" <<  std::scientific << total / double(epoch) << " s./step " << std::endl;
    //printf("I found the following parameters : %e %e %e ; The initial(estimate) parameters being : %e %e %e \n", s.xk->data[4], s.xk->data[5],s.xk->data[6],Wx,Wy,Wz);

    std::cout << " Outputs are saved in imu_ekf*.data " << std::endl;
    std::cout << " You can plot them using e.g. gnuplot : " << std::endl;
    //std::cout << " gnuplot Data/plot-example-009.gplot ; gv Output/example-009-rms.ps ; gv Output/example-009-Lorentz.ps " << std::endl;

    /***********************************************/
    /****            Free the memory            ****/
    /***********************************************/
    ekf_free(p,s);
}
/******************************************************************************************************/


