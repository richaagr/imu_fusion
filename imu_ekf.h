/************************************************************************************************************************************************

********** State:  gyro(x,y,z), quat(x,y,z,w), acc(x,y,z), vel(x,y,z), position(x,y,z), bias(x,y,z)  <------- [19 X 1 vector]  ****************

Code to calculte the position from the given acelero, gyro and orientation data using Kalman Filter

input: 
 		accelerometer data
 		gyrometer data
 		Orientation data 
 		
paramters:
 		1. stabilization_time_of_imu - needs to be set where the bias stops increasing monotonically
 		2. alpha : the low-pass filter parameter. Is the factor with which one trusts the current data
 		3. std_dev
 		4. std_dev_ X / Y / Z
 		5. unaffected_factor -> this is the factor(multiplied by the epsilon ) that tells how much acceleration would not be disturbed by the dynamic bias
 		6. epsilon -> error in acceleration when at rest
		7. epsilon2 -> error in velocity when at rest
		8. epsilon3 -> for error in v*a
		9. no_of_biaspredictor -> used these many datsets to calculte the static bias for gyroscope
 		
output file:
 		in easykf/build/examples:
 			1. imu_ekf.data -> contains the states. ::::::::::::52-54(x,y,z):::::::::::::
 			2. imu_ekf-rms.data
 			
to compile: 
 		cd build/
 		build/ cmake ..
 		build/ make
 		build/ cd examples
 		build/examples/ ./imu_ekf
 		PLOT:
 			gnuplot 
 			plot for[i=2:4] 'imu_ekf.data' using i with lines

Processing with Gyro data:
        Gyro data is simply integrated to get rotations in euler angles.
        Only the static bias is taken care of. This is done by taking the mean of the euler angles that we get in the first few readings and then subtracting it
        from the rest of the readings.

Processing with Acc. data:
    We do the following processing/filtering with the accelerometer readings:
    a. Model the bias (explained below)
    b. Filter (raw) acc. data : removes the bias from the accelerometer readings.
    c. After removing the bias, we further remove the errors by applying a low-pass filter
    d. Post this, we smoothen the acceleration near zero.
    e. Once this processing is done, we calculte the velocity.
    f. Thsi velocity is processed once further.
    g. From this velocity, we calculate the position.

    On bias modelling:
        * we first set a bias_stabilization_time. Till this time, the bias increases monotonically. It is based on the assumption that the bias increases for a first
            seconds and then stabilizes. 
        * Rate of change of bias equals a normal distribution.
        * After the stabilization time, bias only increases if the acceleration if more than a certain threshhold.
        * This threshold equals some factor multiplied by epsilon (epsilon is the range below which we consider acc. to be zero.)
        * The assumption here is that the bias depends, to some extent, on the motion of the phone, i.e., if there is more acceleration, bias would be more and vice-versa.

References (please follow the link):
    * https://ieeexplore.ieee.org/document/6699780/
    * https://ieeexplore.ieee.org/document/6577663/
    * https://www.sciencedirect.com/science/article/pii/S0263224116300689
    * https://www.novatel.com/assets/Documents/Bulletins/APN064.pdf

**********************************************************************************************************************************************************/

#include <cstdlib>
#include <cmath>
#include <iostream>
#include <fstream>
#include <sstream>
#include <sys/time.h>
#include <random>
#include <vector>


#include <easykf.h>

using namespace ekf;

#define DT 0.001208385
double total_time = 0;
#define stabilization_time_of_imu 20.0 //in seconds
#define alpha 0.05
#define gravity 9.81
double mean = 0.0;
double std_dev = sqrt(0.0087923603);// 0.0087923603; //minus-half slope
double std_dev_bias_X = 100.0;//.1;//sqrt(0.0025637051);//0.0025637051; //plus-half slope -> green
double std_dev_bias_Y = 100.0;//.1;//sqrt(0.0025637051);//sqrt(0.0087923603);//0.0087923603;//0.0025637051; //plus-half slope
double std_dev_bias_Z = 100.0;//sqrt(0.0025637051);//0.0087923603;//0.0025637051; //plus-half slope -> blue

#define unaffected_factor 1// this is the factor(multiplied by the epsilon ) that tells how much acceleration would not be disturbed by the dynamic bias
#define epsilon 0.08 //error in acceleration when at rest
#define epsilon2 1e-3//0.002//error in velocity when at rest
#define epsilon3 1e-2//0.0004//for errorin v*a

#define radiantodegree 57.2958

#define WX0 0.1
#define WY0 0.1
#define WZ0 0.1

#define QX0 0
#define QY0 0
#define QZ0 0
#define QW0 1

#define AX0 0.0
#define AY0 0.0
#define AZ0 0.0
#define VX0 0.0
#define VY0 0.0
#define VZ0 0.0
#define PX0 0.0
#define PY0 0.0
#define PZ0 0.0
#define bx0 0.0
#define by0 0.0
#define bz0 0.0

#define NOISE_AMPLITUDE 0.001

#define no_of_biaspredictor 300
//#define no_of_iterations 15800
#define VERBOSE true

#ifndef M_PI
#define M_PI       3.14159265358979323846264338328      /* pi */
#endif

//path for accelerometer-data-RAW
/*
std::string path00 = "/home/gaurav/Desktop/Kalman_filter/easykf/build/examples/Accelero_Processed_2018-04-18-20-28-45-460.csv";
std::ifstream acc_infile_raw(path00);
*/

//path for accelerometer-data-PROCESSED
std::string path0 = "/home/gaurav/Desktop/Kalman_filter/comparison/Set5-random/2018_06_20_13_03_51_246/acc_processed.txt";
std::ifstream acc_infile(path0);

//path for Gyro-data
std::string path1 = "/home/gaurav/Desktop/Kalman_filter/comparison/Set5-random/2018_06_20_13_03_51_246/gyro_raw.txt";
std::ifstream infile(path1);

//path for Orientation
std::string path3 = "/home/gaurav/Desktop/Kalman_filter/comparison/Set5-random/2018_06_20_13_03_51_246/quat.csv";
std::ifstream or_infile(path3);

//path for Gyro-processed-data from device
/*
std::string path2 = "/home/gaurav/Desktop/Kalman_filter/easykf/build/examples/Gyro_Processed_2018-04-04-15-57-31-780.csv";//mystream_3_26_15_41_2,mystream_3_27_18_47_23
std::ifstream processed_infile(path2);
*/


// const double T0 = 0.055013854;
// double t0 =0.055013854;
//const double T0 = t0;

void error(std::string s)
{
  throw std::runtime_error (s);
}

class Mat
{public:
	int rows;
	int cols;
	std::vector<std::vector<float>> element;

	Mat(int rows_, int cols_):rows(rows_), cols(cols_)
	{	
		element.resize(rows_);
		for(int i = 0; i<rows_; i++)
		{
			element[i].resize(cols_);
			for(int j = 0; j<cols_;j++)
				element[i][j] = 0;
		}
	}

    Mat tr()//transpose
	{
		Mat transposed_matrix(cols,rows);
		for(int i = 0; i<rows; i++)
		for(int j = 0; j<cols; j++)
			transposed_matrix[j][i] = element[i][j];
		return transposed_matrix;
	}

    std::vector<float>& operator[](int i)
    { return element[i]; }

    std::vector<double> operator*(std::vector<double> v)
    {
        std::vector<double> prod(rows);
        if(cols != v.size())
            error("Error::Matrices cannot be multiplied");
        for(int i =0; i<rows;i++)
        {
            double sum = 0;
            for(int k = 0; k<cols; k++)
                sum = sum + element[i][k]*v[k];
            prod[i] = sum;
        }
        return prod;
    }
};


double Normal_distribution(double variable, double mean, double stddev)
{
    double N = (exp(-pow(variable - mean,2.0)/(2*pow(stddev,2.0))))/(stddev*sqrt(2*M_PI));
    return N;
}

/** this conversion uses NASA standard aeroplane conventions as described on page:
*   https://www.euclideanspace.com/maths/geometry/rotations/euler/index.htm
*   Coordinate System: right hand
*   Positive angle: right hand
*   Order of euler angles: heading first, then attitude, then bank
*   matrix row column ordering:
*   [m00 m01 m02]
*   [m10 m11 m12]
*   [m20 m21 m22]*/
static void EulertoRotMAt(Mat& m, double heading, double attitude, double bank) {
    // Assuming the angles are in radians.
    double ch = std::cos(heading);
    double sh = std::sin(heading);
    double ca = std::cos(attitude);
    double sa = std::sin(attitude);
    double cb = std::cos(bank);
    double sb = std::sin(bank);

    m[0][0] = ch * ca;
    m[0][1] = ch*sa; //sh*sb - ch*sa*cb;
    m[0][2] = -sh; //ch*sa*sb + sh*cb;
    m[1][0] = sb*sh*ca - cb*sa;//sa;
    m[1][1] = sb*sh*sa + cb*ca;//ca*cb;
    m[1][2] = sb*ch;//-ca*sb;
    m[2][0] = cb*sh*ca + sb*sa;//-sh*ca;
    m[2][1] = cb*sh*sa - sb*ca;//sh*sa*cb + ch*sb;
    m[2][2] = cb*ch;//-sh*sa*sb + ch*cb;

}


double angle_bw_quaternions(const double& q_x,const double& q_y,const double& q_z,const double& q_w,
                            const double& q_x1,const double& q_y1,const double& q_z1,const double& q_w1)
{
	double inner_prod = q_w*q_w1 + q_x*q_x1 + q_y*q_y1 + q_z*q_z1 ;
	return 2.0*acos(inner_prod);

}
void convert_acc(double & ax, double & ay, double & az, double bank, double heading, double attitude)
{
    //bank -> rot. abt x-axis
    //heading -> rot. abt y-axis
    //attitude -> rot. abt z-axis
  
    /* 
    get the euler angles(in radians) from orientation then convert
    those euler to rotation matrix. Use that rotation matrix to
    change the acceleration which we get from imu(in imu's frame
    of reference) to world's frame of reference.
    */
    Mat R(3,3);
    //std::cout<<"\n";
    //std::cout<<"angles = "<<heading<<" "<<attitude<<" "<<bank<<"\n";
    EulertoRotMAt(R, heading, attitude, bank);
   
    std::vector<double> acc(3);
    acc[0] = ax;
    acc[1] = ay;
    acc[2] = az;
    Mat R_transpose = R.tr();
    acc = R * acc; //because if the acceleration vector has been rotated by some angle, then to bring it back we need to muliply it by R_inverse
    ax = acc[0];
    ay = acc[1];
    az = acc[2];
}


void reduce_vel_err(double& velx, double& vely, double& velz, double ax, double ay, double az)
{
    //std::cout<<"VelX= "<<std::abs(velx)<<" epsilon2 = "<<epsilon2<<" ax = "<<ax<<" velx*ax = "<<std::abs(ax)*std::abs(velx)<<" epsilon3 = "<<epsilon3<<"\n";
    
    if(std::abs(velx) < epsilon2 && std::abs(ax)*std::abs(velx) < epsilon3)
        velx = 0.0;

    if(std::abs(vely) < epsilon2 && std::abs(ay)*std::abs(vely) < epsilon3)
        vely = 0.0;

    if(std::abs(velz) < epsilon2 && std::abs(az)*std::abs(velz) < epsilon3)
        velz = 0.0;

    // std::cout<<"Velocity, after reduction = "<<velx<<" "<<vely<<" "<<velz<<"\n";
}

void low_pass_filter(double& a_current, double a_prev)
{
    if(std::abs(a_current - a_prev) < epsilon)
        a_current = (1-alpha)*a_prev + alpha*a_current;
    else
		a_current = 0.6*a_prev + 0.4*a_current;
}

void reduce_acc_error(double& a_x,double& a_y,double& a_z,double ax_prev,double ay_prev, double az_prev)
{  
    low_pass_filter(a_x,ax_prev); 
    low_pass_filter(a_y,ay_prev); 
    low_pass_filter(a_z,az_prev);    
}

//to smoothen acc. near the zero
void smoothen_acc(double& a_x,double& a_y,double& a_z)
{
    if(std::abs(a_x) < epsilon)
        a_x = 0.0;

    if(std::abs(a_y) < epsilon)
        a_y = 0.0;

    if(std::abs(a_z) < epsilon)
        a_z = 0.0;
}

//function to read acceleration and angular velocities from input file

void get_acc(double& timestamp, double& a_x, double& a_y, double& a_z);
void get_gyro(double& timestamp, double& omega_x, double& omega_y, double& omega_z);
void get_orientation(double& timestamp, double& omega_x, double& omega_y, double& omega_z, double th_X_0, double th_Y_0, double th_Z_0);


//conversion from quaternion to euler angles roll, pitch and yaw
void toEulerAngle(const double& qx, const double& qy, const double& qz, const double& qw, double& roll, double& pitch, double& yaw)
{
	// roll (x-axis rotation)
	double sinr = +2.0 * (qw * qx + qy * qz);
	double cosr = +1.0 - 2.0 * (qx * qx + qy * qy);
	roll = atan2(sinr, cosr);

	// pitch (y-axis rotation)
	double sinp = +2.0 * (qw * qy - qz * qx);
	if (std::fabs(sinp) >= 1)
		pitch = copysign(M_PI / 2, sinp); // use 90 degrees if out of range
	else
		pitch = asin(sinp);

	// yaw (z-axis rotation)
	double siny = +2.0 * (qw * qz + qx * qy);
	double cosy = +1.0 - 2.0 * (qy * qy + qz * qz);  
	yaw = atan2(siny, cosy);
}


/*************************************************************************************/
/*            Definition of the evolution and observation functions                  */
/*************************************************************************************/

// Evolution function
void f(gsl_vector * params, gsl_vector * xk_1, gsl_vector * xk);

// Jacobian of the evolution function
void df(gsl_vector * params, gsl_vector * xk_1, gsl_matrix * Fxk);

// Observation function
void h(gsl_vector * params, gsl_vector * xk , gsl_vector * yk);

// Jacobian of the observation function
void dh(gsl_vector * params, gsl_vector * xk , gsl_matrix * Hyk);

/*******************************************************************************************************

****************************function definitiion below**************************************************

*******************************************************************************************************/


void get_acc(double& timestamp, double& a_x, double& a_y, double& a_z)
{
    std::string line;
    std::getline(acc_infile,line); //read timestamp, omega_x, omega_y, omega_z
    std::istringstream iss0(line);

    char ch;
    iss0>>timestamp>>ch>>a_x>>ch>>a_y>>ch>>a_z; 
}

void get_gyro(double& timestamp, double& omega_x, double& omega_y, double& omega_z)
{
    std::string line;
    std::getline(infile,line); //read timestamp, omega_x, omega_y, omega_z
    
    std::istringstream iss0(line);

    char ch;

    iss0>>timestamp>>ch>>omega_x>>ch>>omega_y>>ch>>omega_z;
}

void get_orientation(double& timestamp, double& theta_x, double& theta_y, double& theta_z, double th_X_0, double th_Y_0, double th_Z_0)
{
    std::string line;
    std::getline(or_infile,line); //read timestamp, theta_x, theta_y, theta_z
    
    std::istringstream iss0(line);

    char ch;

    iss0>>timestamp>>ch>>theta_z>>ch>>theta_x>>ch>>theta_y;
    
        theta_z -= th_Z_0;

        theta_x -= th_X_0;

        theta_y -= th_Y_0;
    
}

void filter_acc(double& a, double b, double n)
{
    if(a > 0.0)
    {
        if(std::abs(a) > unaffected_factor*epsilon)
        {
            if(b > 0.0)
                a = a - n - b;
            else
                a = a - n + b;
        }
        else
            a = a - n ;
    }
    else
    {
        if(std::abs(a) > unaffected_factor*epsilon)
        {
            if(b > 0.0)
                a = a - n + b;
            else
                a = a - n - b;   
        }
        else
            a = a - n ;
    }
}

void f(gsl_vector * params, gsl_vector * xk_1, gsl_vector * xk)
{    
    double wx = gsl_vector_get(params, 1);
    double wy = gsl_vector_get(params, 2);
    double wz = gsl_vector_get(params, 3);


    double qx = gsl_vector_get(xk_1,3);
    double qy = gsl_vector_get(xk_1,4);
    double qz = gsl_vector_get(xk_1,5);
    double qw = gsl_vector_get(xk_1,6);
                                                //since, we are taking aceleration from parameter data
    double ax = gsl_vector_get(params, 4);
    double ay = gsl_vector_get(params, 5);
    double az = gsl_vector_get(params, 6);

    double vx = gsl_vector_get(xk_1, 10);
    double vy = gsl_vector_get(xk_1, 11);
    double vz = gsl_vector_get(xk_1, 12);

    double px = gsl_vector_get(xk_1, 13);
    double py = gsl_vector_get(xk_1, 14);
    double pz = gsl_vector_get(xk_1, 15);
                                                //bias
    double bx = gsl_vector_get(xk_1, 16);
    double by = gsl_vector_get(xk_1, 17);
    double bz = gsl_vector_get(xk_1, 18);

    double dt = gsl_vector_get(params, 0);

    gsl_vector_set(xk, 0, wx);
    gsl_vector_set(xk, 1, wy);
    gsl_vector_set(xk, 2, wz);
    
    gsl_vector_set(xk, 3, qx + dt * (wz*qy - wy*qz + wx*qw)/2);
    gsl_vector_set(xk, 4, qy + dt * (wx*qz - wz*qx + wy*qw)/2);
    gsl_vector_set(xk, 5, qz + dt * (wy*qx - wx*qy + wz*qw)/2);
    gsl_vector_set(xk, 6, qw + dt * (-wx*qx - wy*qy - wz*qz)/2);



   //If using the processed accelrometer reading, then use this
  
//   std::default_random_engine generator;

    std::random_device rd;

    std::mt19937 generator(rd());
    std::normal_distribution<double> distribution(mean, std_dev);
    std::uniform_int_distribution<int> coinFlip(0,1);
    double nx = distribution(generator);
    double ny = distribution(generator);
    double nz = distribution(generator);

    //std::cout<<"bias random =>"<<nx<<" "<<ny<<" "<<nz<<"\n";

/*************** Accelerometer (dynamic) bias modelling ************************/

    std::normal_distribution<double> distribution_bias_X(mean, std_dev_bias_X);
    std::normal_distribution<double> distribution_bias_Y(mean, std_dev_bias_Y);
    std::normal_distribution<double> distribution_bias_Z(mean, std_dev_bias_Z);
    //double bias_x = distribution_bias(generator);
    if(total_time <= stabilization_time_of_imu)
    {
        double bx_error_increment =  Normal_distribution(ax,mean,std_dev_bias_X) * dt ;
        if(coinFlip(generator)) //(rand()%2==0)
            bx = bx + bx_error_increment;
        else
            bx = bx - bx_error_increment;
    
        double by_error_increment = Normal_distribution(ay,mean,std_dev_bias_Y) * dt ;
        if(rand()%2 == 0)
            by = by + by_error_increment;
        else
            by = by - by_error_increment;
                
        double bz_error_increment =  Normal_distribution(az,mean,std_dev_bias_Z) * dt ;
        if(rand()%2 == 0)
            bz = bz + bz_error_increment;
        else
            bz = bz - bz_error_increment; 
    }
   else
   {
        double bx_error_increment =  Normal_distribution(ax,mean,std_dev_bias_X) * dt ;
        if(std::abs(ax) > unaffected_factor*epsilon)
        if(rand()%2 == 0)
            bx = bx + bx_error_increment;
        else
            bx = bx - bx_error_increment;

        double by_error_increment =  Normal_distribution(ay,mean,std_dev_bias_Y) * dt ;
        if(std::abs(ay) > unaffected_factor*epsilon)
        if(rand()%2 == 0)
            by = by + by_error_increment;
        else
            by = by - by_error_increment;
        
        double bz_error_increment =  Normal_distribution(az,mean,std_dev_bias_Z) * dt ;
        if(std::abs(az) > unaffected_factor*epsilon)
        if(rand()%2 == 0)
            bz = bz + bz_error_increment;
        else
            bz = bz - bz_error_increment;
    }

    double axprev = gsl_vector_get(xk_1, 7);
    double ayprev = gsl_vector_get(xk_1, 8);
    double azprev = gsl_vector_get(xk_1, 9);
    
    
    
    filter_acc(ax,bx,nx);
    filter_acc(ay,by,ny);
    filter_acc(az,bz,nz);
    
    reduce_acc_error(ax,ay,az,axprev,ayprev,azprev);
    smoothen_acc(ax,ay,az);

    gsl_vector_set(xk, 7, ax);
    gsl_vector_set(xk, 8, ay);
    gsl_vector_set(xk, 9, az);

    double velx = 0.0, vely = 0.0, velz = 0.0;
    velx = vx + ax*dt;//(ax-nx-bx)*dt;
    vely = vy + ay*dt;//(ay-ny-by)*dt;
    velz = vz + az*dt;//(az-nz-bz)*dt;
    
    reduce_vel_err(velx, vely, velz, ax, ay, az);
    

    gsl_vector_set(xk, 10, velx);
    gsl_vector_set(xk, 11, vely);
    gsl_vector_set(xk, 12, velz);

    gsl_vector_set(xk, 13, px + velx*dt - 0.5*ax*dt*dt);//(ax-nx-bx)*dt*dt);
    gsl_vector_set(xk, 14, py + vely*dt - 0.5*ay*dt*dt);//(ay-ny-by)*dt*dt);
    gsl_vector_set(xk, 15, pz + velz*dt - 0.5*az*dt*dt);//(az-nz-bz)*dt*dt);
    
    gsl_vector_set(xk, 16, bx);
    gsl_vector_set(xk, 17, by);
    gsl_vector_set(xk, 18, bz);
}


void df(gsl_vector * params, gsl_vector * xk_1, gsl_matrix* Fxk)
{
    double wx = gsl_vector_get(xk_1, 0);
    double wy = gsl_vector_get(xk_1, 1);
    double wz = gsl_vector_get(xk_1, 2);

    double qx = gsl_vector_get(xk_1,3);
    double qy = gsl_vector_get(xk_1,4);
    double qz = gsl_vector_get(xk_1,5);
    double qw = gsl_vector_get(xk_1,6);
                                                //since, we are taking aceleration from parameter data
    double ax = gsl_vector_get(params, 4);
    double ay = gsl_vector_get(params, 5);
    double az = gsl_vector_get(params, 6);


    double vx = gsl_vector_get(xk_1, 10);
    double vy = gsl_vector_get(xk_1, 11);
    double vz = gsl_vector_get(xk_1, 12);

    double px = gsl_vector_get(xk_1, 13);
    double py = gsl_vector_get(xk_1, 14);
    double pz = gsl_vector_get(xk_1, 15);
                                                //bias
    double bx = gsl_vector_get(xk_1, 16);
    double by = gsl_vector_get(xk_1, 17);
    double bz = gsl_vector_get(xk_1, 18);

    double dt = gsl_vector_get(params, 0);

    // Derivatives for wx(t+1) = ..
    gsl_matrix_set(Fxk, 0, 0, 1.0);
    gsl_matrix_set(Fxk, 0, 1, 0.0);
    gsl_matrix_set(Fxk, 0, 2, 0.0);
    gsl_matrix_set(Fxk, 0, 3, qw*dt/2);
    gsl_matrix_set(Fxk, 0, 4, qz*dt/2);
    gsl_matrix_set(Fxk, 0, 5, -qy*dt/2);
    gsl_matrix_set(Fxk, 0, 6, -qx*dt/2);
    gsl_matrix_set(Fxk, 0, 7, 0.0);
    gsl_matrix_set(Fxk, 0, 8, 0.0);
    gsl_matrix_set(Fxk, 0, 9, 0.0);
    gsl_matrix_set(Fxk, 0, 10, 0.0);
    gsl_matrix_set(Fxk, 0, 11, 0.0);
    gsl_matrix_set(Fxk, 0, 12, 0.0);
    gsl_matrix_set(Fxk, 0, 13, 0.0);
    gsl_matrix_set(Fxk, 0, 14, 0.0);
    gsl_matrix_set(Fxk, 0, 15, 0.0);
    gsl_matrix_set(Fxk, 0, 16, 0.0);
    gsl_matrix_set(Fxk, 0, 17, 0.0);
    gsl_matrix_set(Fxk, 0, 18, 0.0);

    // Derivatives for wy(t+1) = ..
    gsl_matrix_set(Fxk, 1, 0, 0.0);
    gsl_matrix_set(Fxk, 1, 1, 1.0);
    gsl_matrix_set(Fxk, 1, 2, 0.0);
    gsl_matrix_set(Fxk, 1, 3, -qz*dt/2);
    gsl_matrix_set(Fxk, 1, 4, qw*dt/2);
    gsl_matrix_set(Fxk, 1, 5, qx*dt/2);
    gsl_matrix_set(Fxk, 1, 6, -qy*dt/2);
    gsl_matrix_set(Fxk, 1, 7, 0.0);
    gsl_matrix_set(Fxk, 1, 8, 0.0);
    gsl_matrix_set(Fxk, 1, 9, 0.0);
    gsl_matrix_set(Fxk, 1, 10, 0.0);
    gsl_matrix_set(Fxk, 1, 11, 0.0);
    gsl_matrix_set(Fxk, 1, 12, 0.0);
    gsl_matrix_set(Fxk, 1, 13, 0.0);
    gsl_matrix_set(Fxk, 1, 14, 0.0);
    gsl_matrix_set(Fxk, 1, 15, 0.0);
    gsl_matrix_set(Fxk, 1, 16, 0.0);
    gsl_matrix_set(Fxk, 1, 17, 0.0);
    gsl_matrix_set(Fxk, 1, 18, 0.0);

    // Derivatives for wz(t+1) = ..
    gsl_matrix_set(Fxk, 2, 0, 0.0);
    gsl_matrix_set(Fxk, 2, 1, 0.0);
    gsl_matrix_set(Fxk, 2, 2, 1.0);
    gsl_matrix_set(Fxk, 2, 3, qy*dt/2);
    gsl_matrix_set(Fxk, 2, 4, -qx*dt/2);
    gsl_matrix_set(Fxk, 2, 5, qw*dt/2);
    gsl_matrix_set(Fxk, 2, 6, -qz*dt/2);
    gsl_matrix_set(Fxk, 2, 7, 0.0);
    gsl_matrix_set(Fxk, 2, 8, 0.0);
    gsl_matrix_set(Fxk, 2, 9, 0.0);
    gsl_matrix_set(Fxk, 2, 10, 0.0);
    gsl_matrix_set(Fxk, 2, 11, 0.0);
    gsl_matrix_set(Fxk, 2, 12, 0.0);
    gsl_matrix_set(Fxk, 2, 13, 0.0);
    gsl_matrix_set(Fxk, 2, 14, 0.0);
    gsl_matrix_set(Fxk, 2, 15, 0.0);
    gsl_matrix_set(Fxk, 2, 16, 0.0);
    gsl_matrix_set(Fxk, 2, 17, 0.0);
    gsl_matrix_set(Fxk, 2, 18, 0.0);


    // Derivatives for qx(t+1) = ..
    gsl_matrix_set(Fxk, 3, 0, 0.0);
    gsl_matrix_set(Fxk, 3, 1, 0.0);
    gsl_matrix_set(Fxk, 3, 2, 0.0);
    gsl_matrix_set(Fxk, 3, 3, 1.0);
    gsl_matrix_set(Fxk, 3, 4, -wz*dt/2);
    gsl_matrix_set(Fxk, 3, 5, wy*dt/2);
    gsl_matrix_set(Fxk, 3, 6, -wx*dt/2);

//when using process data, use this:
    gsl_matrix_set(Fxk, 3, 7, 0.0);
    gsl_matrix_set(Fxk, 3, 8, 0.0);
    gsl_matrix_set(Fxk, 3, 9, 0.0);

    gsl_matrix_set(Fxk, 3, 10, 0.0);
    gsl_matrix_set(Fxk, 3, 11, 0.0);
    gsl_matrix_set(Fxk, 3, 12, 0.0);

    gsl_matrix_set(Fxk, 3, 13, 0.0);
    gsl_matrix_set(Fxk, 3, 14, 0.0);
    gsl_matrix_set(Fxk, 3, 15, 0.0);

    gsl_matrix_set(Fxk, 3, 16, 0.0);
    gsl_matrix_set(Fxk, 3, 17, 0.0);
    gsl_matrix_set(Fxk, 3, 18, 0.0);

    // Derivatives for qy(t+1) = ..
    gsl_matrix_set(Fxk, 4, 0, 0.0);
    gsl_matrix_set(Fxk, 4, 1, 0.0);
    gsl_matrix_set(Fxk, 4, 2, 0.0);
    gsl_matrix_set(Fxk, 4, 3, wz*dt/2);
    gsl_matrix_set(Fxk, 4, 4, 1.0);
    gsl_matrix_set(Fxk, 4, 5, -wx*dt/2);
    gsl_matrix_set(Fxk, 4, 6, -wy*dt/2);


//when using process data, use this:
    gsl_matrix_set(Fxk, 4, 7, 0.0);
    gsl_matrix_set(Fxk, 4, 8, 0.0);
    gsl_matrix_set(Fxk, 4, 9, 0.0);

    gsl_matrix_set(Fxk, 4, 10, 0.0);
    gsl_matrix_set(Fxk, 4, 11, 0.0);
    gsl_matrix_set(Fxk, 4, 12, 0.0);

    gsl_matrix_set(Fxk, 4, 13, 0.0);
    gsl_matrix_set(Fxk, 4, 14, 0.0);
    gsl_matrix_set(Fxk, 4, 15, 0.0);

    gsl_matrix_set(Fxk, 4, 16, 0.0);
    gsl_matrix_set(Fxk, 4, 17, 0.0);
    gsl_matrix_set(Fxk, 4, 18, 0.0);


    // Derivatives for qz(t+1) = ..
    gsl_matrix_set(Fxk, 5, 0, 0.0);
    gsl_matrix_set(Fxk, 5, 1, 0.0);
    gsl_matrix_set(Fxk, 5, 2, 0.0);
    gsl_matrix_set(Fxk, 5, 3, -wy*dt/2);
    gsl_matrix_set(Fxk, 5, 4, wx*dt/2);
    gsl_matrix_set(Fxk, 5, 5, 1.0);
    gsl_matrix_set(Fxk, 5, 6, -wz*dt/2);


//when using process data, use this:
    gsl_matrix_set(Fxk, 5, 7, 0.0);
    gsl_matrix_set(Fxk, 5, 8, 0.0);
    gsl_matrix_set(Fxk, 5, 9, 0.0);

    gsl_matrix_set(Fxk, 5, 10, 0.0);
    gsl_matrix_set(Fxk, 5, 11, 0.0);
    gsl_matrix_set(Fxk, 5, 12, 0.0);

    gsl_matrix_set(Fxk, 5, 13, 0.0);
    gsl_matrix_set(Fxk, 5, 14, 0.0);
    gsl_matrix_set(Fxk, 5, 15, 0.0);

    gsl_matrix_set(Fxk, 5, 16, 0.0);
    gsl_matrix_set(Fxk, 5, 17, 0.0);
    gsl_matrix_set(Fxk, 5, 18, 0.0);

    // Derivatives for qw(t+1) = ..
    gsl_matrix_set(Fxk, 6, 0, 0.0);
    gsl_matrix_set(Fxk, 6, 1, 0.0);
    gsl_matrix_set(Fxk, 6, 2, 0.0);
    gsl_matrix_set(Fxk, 6, 3, wx*dt/2);
    gsl_matrix_set(Fxk, 6, 4, wy*dt/2);
    gsl_matrix_set(Fxk, 6, 5, wz*dt/2);
    gsl_matrix_set(Fxk, 6, 6, 1.0);


//when using process data, use this:
    gsl_matrix_set(Fxk, 6, 7, 0.0);
    gsl_matrix_set(Fxk, 6, 8, 0.0);
    gsl_matrix_set(Fxk, 6, 9, 0.0);

    gsl_matrix_set(Fxk, 6, 10, 0.0);
    gsl_matrix_set(Fxk, 6, 11, 0.0);
    gsl_matrix_set(Fxk, 6, 12, 0.0);

    gsl_matrix_set(Fxk, 6, 13, 0.0);
    gsl_matrix_set(Fxk, 6, 14, 0.0);
    gsl_matrix_set(Fxk, 6, 15, 0.0);

    gsl_matrix_set(Fxk, 6, 16, 0.0);
    gsl_matrix_set(Fxk, 6, 17, 0.0);
    gsl_matrix_set(Fxk, 6, 18, 0.0);

    //Derivative for ax(t+1)
    gsl_matrix_set(Fxk, 7, 0, 0.0);
    gsl_matrix_set(Fxk, 7, 1, 0.0);
    gsl_matrix_set(Fxk, 7, 2, 0.0);
    gsl_matrix_set(Fxk, 7, 3, 0.0);
    gsl_matrix_set(Fxk, 7, 4, 0.0);
    gsl_matrix_set(Fxk, 7, 5, 0.0);
    gsl_matrix_set(Fxk, 7, 6, 0.0);

    gsl_matrix_set(Fxk, 7, 7, 1.0);
    gsl_matrix_set(Fxk, 7, 8, 0.0);
    gsl_matrix_set(Fxk, 7, 9, 0.0);

    gsl_matrix_set(Fxk, 7, 10, dt);
    gsl_matrix_set(Fxk, 7, 11, 0.0);
    gsl_matrix_set(Fxk, 7, 12, 0.0);

    gsl_matrix_set(Fxk, 7, 13, 0.5*dt*dt);
    gsl_matrix_set(Fxk, 7, 14, 0.0);
    gsl_matrix_set(Fxk, 7, 15, 0.0);

    gsl_matrix_set(Fxk, 7, 16, 0.0);
    gsl_matrix_set(Fxk, 7, 17, 0.0);
    gsl_matrix_set(Fxk, 7, 18, 0.0);


    //Derivative for ay(t+1)
    gsl_matrix_set(Fxk, 8, 0, 0.0);
    gsl_matrix_set(Fxk, 8, 1, 0.0);
    gsl_matrix_set(Fxk, 8, 2, 0.0);
    gsl_matrix_set(Fxk, 8, 3, 0.0);
    gsl_matrix_set(Fxk, 8, 4, 0.0);
    gsl_matrix_set(Fxk, 8, 5, 0.0);
    gsl_matrix_set(Fxk, 8, 6, 0.0);

    gsl_matrix_set(Fxk, 8, 7, 0.0);
    gsl_matrix_set(Fxk, 8, 8, 1.0);
    gsl_matrix_set(Fxk, 8, 9, 0.0);

    gsl_matrix_set(Fxk, 8, 10, 0.0);
    gsl_matrix_set(Fxk, 8, 11, dt);
    gsl_matrix_set(Fxk, 8, 12, 0.0);

    gsl_matrix_set(Fxk, 8, 13, 0.0);
    gsl_matrix_set(Fxk, 8, 14, 0.5*dt*dt);
    gsl_matrix_set(Fxk, 8, 15, 0.0);

    gsl_matrix_set(Fxk, 8, 16, 0.0);
    gsl_matrix_set(Fxk, 8, 17, 0.0);
    gsl_matrix_set(Fxk, 8, 18, 0.0);
    
    //Derivative for az(t+1)
    gsl_matrix_set(Fxk, 9, 0, 0.0);
    gsl_matrix_set(Fxk, 9, 1, 0.0);
    gsl_matrix_set(Fxk, 9, 2, 0.0);
    gsl_matrix_set(Fxk, 9, 3, 0.0);
    gsl_matrix_set(Fxk, 9, 4, 0.0);
    gsl_matrix_set(Fxk, 9, 5, 0.0);
    gsl_matrix_set(Fxk, 9, 6, 0.0);

    gsl_matrix_set(Fxk, 9, 7, 0.0);
    gsl_matrix_set(Fxk, 9, 8, 0.0);
    gsl_matrix_set(Fxk, 9, 9, 1.0);

    gsl_matrix_set(Fxk, 9, 10, 0.0);
    gsl_matrix_set(Fxk, 9, 11, 0.0);
    gsl_matrix_set(Fxk, 9, 12, dt);

    gsl_matrix_set(Fxk, 9, 13, 0.0);
    gsl_matrix_set(Fxk, 9, 14, 0.0);
    gsl_matrix_set(Fxk, 9, 15, 0.5*dt*dt);

    gsl_matrix_set(Fxk, 9, 16, 0.0);
    gsl_matrix_set(Fxk, 9, 17, 0.0);
    gsl_matrix_set(Fxk, 9, 18, 0.0);

    //Derivative for vx(t+1)
    gsl_matrix_set(Fxk, 10, 0, 0.0);
    gsl_matrix_set(Fxk, 10, 1, 0.0);
    gsl_matrix_set(Fxk, 10, 2, 0.0);
    gsl_matrix_set(Fxk, 10, 3, 0.0);
    gsl_matrix_set(Fxk, 10, 4, 0.0);
    gsl_matrix_set(Fxk, 10, 5, 0.0);
    gsl_matrix_set(Fxk, 10, 6, 0.0);

    gsl_matrix_set(Fxk, 10, 7, 0.0);
    gsl_matrix_set(Fxk, 10, 8, 0.0);
    gsl_matrix_set(Fxk, 10, 9, 0.0);

    gsl_matrix_set(Fxk, 10, 10, 1.0);
    gsl_matrix_set(Fxk, 10, 11, 0.0);
    gsl_matrix_set(Fxk, 10, 12, 0.0);

    gsl_matrix_set(Fxk, 10, 13, dt);
    gsl_matrix_set(Fxk, 10, 14, 0.0);
    gsl_matrix_set(Fxk, 10, 15, 0.0);
 
    gsl_matrix_set(Fxk, 10, 16, 0.0);
    gsl_matrix_set(Fxk, 10, 17, 0.0);
    gsl_matrix_set(Fxk, 10, 18, 0.0);

    //Derivative for vy(t+1)
    gsl_matrix_set(Fxk, 11, 0, 0.0);
    gsl_matrix_set(Fxk, 11, 1, 0.0);
    gsl_matrix_set(Fxk, 11, 2, 0.0);
    gsl_matrix_set(Fxk, 11, 3, 0.0);
    gsl_matrix_set(Fxk, 11, 4, 0.0);
    gsl_matrix_set(Fxk, 11, 5, 0.0);
    gsl_matrix_set(Fxk, 11, 6, 0.0);

    gsl_matrix_set(Fxk, 11, 7, 0.0);
    gsl_matrix_set(Fxk, 11, 8, 0.0);
    gsl_matrix_set(Fxk, 11, 9, 0.0);

    gsl_matrix_set(Fxk, 11, 10, 0.0);
    gsl_matrix_set(Fxk, 11, 11, 1.0);
    gsl_matrix_set(Fxk, 11, 12, 0.0);

    gsl_matrix_set(Fxk, 11, 13, 0.0);
    gsl_matrix_set(Fxk, 11, 14, dt);
    gsl_matrix_set(Fxk, 11, 15, 0.0);

    gsl_matrix_set(Fxk, 11, 16, 0.0);
    gsl_matrix_set(Fxk, 11, 17, 0.0);
    gsl_matrix_set(Fxk, 11, 18, 0.0);

    //Derivative for vz(t+1)
    gsl_matrix_set(Fxk, 12, 0, 0.0);
    gsl_matrix_set(Fxk, 12, 1, 0.0);
    gsl_matrix_set(Fxk, 12, 2, 0.0);
    gsl_matrix_set(Fxk, 12, 3, 0.0);
    gsl_matrix_set(Fxk, 12, 4, 0.0);
    gsl_matrix_set(Fxk, 12, 5, 0.0);
    gsl_matrix_set(Fxk, 12, 6, 0.0);

    gsl_matrix_set(Fxk, 12, 7, 0.0);
    gsl_matrix_set(Fxk, 12, 8, 0.0);
    gsl_matrix_set(Fxk, 12, 9, 0.0);

    gsl_matrix_set(Fxk, 12, 10, 0.0);
    gsl_matrix_set(Fxk, 12, 11, 0.0);
    gsl_matrix_set(Fxk, 12, 12, 1.0);

    gsl_matrix_set(Fxk, 12, 13, 0.0);
    gsl_matrix_set(Fxk, 12, 14, 0.0);
    gsl_matrix_set(Fxk, 12, 15, dt);

    gsl_matrix_set(Fxk, 12, 16, 0.0);
    gsl_matrix_set(Fxk, 12, 17, 0.0);
    gsl_matrix_set(Fxk, 12, 18, 0.0);

    //Derivative for px(t+1)
    gsl_matrix_set(Fxk, 13, 0, 0.0);
    gsl_matrix_set(Fxk, 13, 1, 0.0);
    gsl_matrix_set(Fxk, 13, 2, 0.0);
    gsl_matrix_set(Fxk, 13, 3, 0.0);
    gsl_matrix_set(Fxk, 13, 4, 0.0);
    gsl_matrix_set(Fxk, 13, 5, 0.0);
    gsl_matrix_set(Fxk, 13, 6, 0.0);

    gsl_matrix_set(Fxk, 13, 7, 0.0);
    gsl_matrix_set(Fxk, 13, 8, 0.0);
    gsl_matrix_set(Fxk, 13, 9, 0.0);

    gsl_matrix_set(Fxk, 13, 10, 0.0);
    gsl_matrix_set(Fxk, 13, 11, 0.0);
    gsl_matrix_set(Fxk, 13, 12, 0.0);

    gsl_matrix_set(Fxk, 13, 13, 1.0);
    gsl_matrix_set(Fxk, 13, 14, 0.0);
    gsl_matrix_set(Fxk, 13, 15, 0.0);

    gsl_matrix_set(Fxk, 13, 16, 0.0);
    gsl_matrix_set(Fxk, 13, 17, 0.0);
    gsl_matrix_set(Fxk, 13, 18, 0.0);

    //Derivative for py(t+1)
    gsl_matrix_set(Fxk, 14, 0, 0.0);
    gsl_matrix_set(Fxk, 14, 1, 0.0);
    gsl_matrix_set(Fxk, 14, 2, 0.0);
    gsl_matrix_set(Fxk, 14, 3, 0.0);
    gsl_matrix_set(Fxk, 14, 4, 0.0);
    gsl_matrix_set(Fxk, 14, 5, 0.0);
    gsl_matrix_set(Fxk, 14, 6, 0.0);

    gsl_matrix_set(Fxk, 14, 7, 0.0);
    gsl_matrix_set(Fxk, 14, 8, 0.0);
    gsl_matrix_set(Fxk, 14, 9, 0.0);

    gsl_matrix_set(Fxk, 14, 10, 0.0);
    gsl_matrix_set(Fxk, 14, 11, 0.0);
    gsl_matrix_set(Fxk, 14, 12, 0.0);

    gsl_matrix_set(Fxk, 14, 13, 0.0);
    gsl_matrix_set(Fxk, 14, 14, 1.0);
    gsl_matrix_set(Fxk, 14, 15, 0.0);

    gsl_matrix_set(Fxk, 14, 16, 0.0);
    gsl_matrix_set(Fxk, 14, 17, 0.0);
    gsl_matrix_set(Fxk, 14, 18, 0.0);

    //Derivative for pz(t+1)
    gsl_matrix_set(Fxk, 15, 0, 0.0);
    gsl_matrix_set(Fxk, 15, 1, 0.0);
    gsl_matrix_set(Fxk, 15, 2, 0.0);
    gsl_matrix_set(Fxk, 15, 3, 0.0);
    gsl_matrix_set(Fxk, 15, 4, 0.0);
    gsl_matrix_set(Fxk, 15, 5, 0.0);
    gsl_matrix_set(Fxk, 15, 6, 0.0);

    gsl_matrix_set(Fxk, 15, 7, 0.0);
    gsl_matrix_set(Fxk, 15, 8, 0.0);
    gsl_matrix_set(Fxk, 15, 9, 0.0);

    gsl_matrix_set(Fxk, 15, 10, 0.0);
    gsl_matrix_set(Fxk, 15, 11, 0.0);
    gsl_matrix_set(Fxk, 15, 12, 0.0);

    gsl_matrix_set(Fxk, 15, 13, 0.0);
    gsl_matrix_set(Fxk, 15, 14, 0.0);
    gsl_matrix_set(Fxk, 15, 15, 1.0);

    gsl_matrix_set(Fxk, 15, 16, 0.0);
    gsl_matrix_set(Fxk, 15, 17, 0.0);
    gsl_matrix_set(Fxk, 15, 18, 0.0);

    //Derivative for bx(t+1)
    gsl_matrix_set(Fxk, 16, 0, 0.0);
    gsl_matrix_set(Fxk, 16, 1, 0.0);
    gsl_matrix_set(Fxk, 16, 2, 0.0);
    gsl_matrix_set(Fxk, 16, 3, 0.0);
    gsl_matrix_set(Fxk, 16, 4, 0.0);
    gsl_matrix_set(Fxk, 16, 5, 0.0);
    gsl_matrix_set(Fxk, 16, 6, 0.0);

    gsl_matrix_set(Fxk, 16, 7, -1.0);
    gsl_matrix_set(Fxk, 16, 8, 0.0);
    gsl_matrix_set(Fxk, 16, 9, 0.0);

    gsl_matrix_set(Fxk, 16, 10, -dt);
    gsl_matrix_set(Fxk, 16, 11, 0.0);
    gsl_matrix_set(Fxk, 16, 12, 0.0);

    gsl_matrix_set(Fxk, 16, 13, -0.5*dt*dt);
    gsl_matrix_set(Fxk, 16, 14, 0.0);
    gsl_matrix_set(Fxk, 16, 15, 0.0);

    gsl_matrix_set(Fxk, 16, 16, 1.0);
    gsl_matrix_set(Fxk, 16, 17, 0.0);
    gsl_matrix_set(Fxk, 16, 18, 0.0);

    //Derivative for by(t+1)
    gsl_matrix_set(Fxk, 17, 0, 0.0);
    gsl_matrix_set(Fxk, 17, 1, 0.0);
    gsl_matrix_set(Fxk, 17, 2, 0.0);
    gsl_matrix_set(Fxk, 17, 3, 0.0);
    gsl_matrix_set(Fxk, 17, 4, 0.0);
    gsl_matrix_set(Fxk, 17, 5, 0.0);
    gsl_matrix_set(Fxk, 17, 6, 0.0);

    gsl_matrix_set(Fxk, 17, 7, 0.0);
    gsl_matrix_set(Fxk, 17, 8, -1.0);
    gsl_matrix_set(Fxk, 17, 9, 0.0);

    gsl_matrix_set(Fxk, 17, 10, 0.0);
    gsl_matrix_set(Fxk, 17, 11, -dt);
    gsl_matrix_set(Fxk, 17, 12, 0.0);

    gsl_matrix_set(Fxk, 17, 13, 0.0);
    gsl_matrix_set(Fxk, 17, 14, -0.5*dt*dt);
    gsl_matrix_set(Fxk, 17, 15, 0.0);

    gsl_matrix_set(Fxk, 17, 16, 0.0);
    gsl_matrix_set(Fxk, 17, 17, 1.0);
    gsl_matrix_set(Fxk, 17, 18, 0.0);

    //Derivative for bz(t+1)
    gsl_matrix_set(Fxk, 18, 0, 0.0);
    gsl_matrix_set(Fxk, 18, 1, 0.0);
    gsl_matrix_set(Fxk, 18, 2, 0.0);
    gsl_matrix_set(Fxk, 18, 3, 0.0);
    gsl_matrix_set(Fxk, 18, 4, 0.0);
    gsl_matrix_set(Fxk, 18, 5, 0.0);
    gsl_matrix_set(Fxk, 18, 6, 0.0);

    gsl_matrix_set(Fxk, 18, 7, 0.0);
    gsl_matrix_set(Fxk, 18, 8, 0.0);
    gsl_matrix_set(Fxk, 18, 9, -1.0);

    gsl_matrix_set(Fxk, 18, 10, 0.0);
    gsl_matrix_set(Fxk, 18, 11, 0.0);
    gsl_matrix_set(Fxk, 18, 12, -dt);

    gsl_matrix_set(Fxk, 18, 13, 0.0);
    gsl_matrix_set(Fxk, 18, 14, 0.0);
    gsl_matrix_set(Fxk, 18, 15, -0.5*dt*dt);

    gsl_matrix_set(Fxk, 18, 16, 0.0);
    gsl_matrix_set(Fxk, 18, 17, 0.0);
    gsl_matrix_set(Fxk, 18, 18, 1.0);


}

void h(gsl_vector * params, gsl_vector * xk , gsl_vector * yk)
{
    //for(unsigned int i = 0 ; i < yk->size ; ++i)
      for(unsigned int i = 0 ; i < yk->size ; ++i)
        gsl_vector_set(yk, i, gsl_vector_get(xk,i) + NOISE_AMPLITUDE*rand()/ double(RAND_MAX));

      gsl_vector_set(yk, 0, gsl_vector_get(params,1));
      gsl_vector_set(yk, 1, gsl_vector_get(params,2));
      gsl_vector_set(yk, 2, gsl_vector_get(params,3));
      gsl_vector_set(yk, 7, gsl_vector_get(params,4));
      gsl_vector_set(yk, 8, gsl_vector_get(params,5));
      gsl_vector_set(yk, 9, gsl_vector_get(params,6));
      
}

void dh(gsl_vector * params, gsl_vector * xk , gsl_matrix * Hyk)
{
    double dt = gsl_vector_get(params, 0);
    
    gsl_matrix_set_zero(Hyk);
    for(unsigned int i = 0; i < xk->size; ++i)
        gsl_matrix_set(Hyk, i, i, 1.0);
}


